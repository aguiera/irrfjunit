package IRRF;

public class FaixaIR {
	private int ano;
	private int faixa;
	private double minimo;
	private double maximo;
	private double deducao;
	private double aliquota;
	
	public FaixaIR(int ano, int faixa, double minimo, double maximo, double deducao, double aliquota) {
		super();
		this.ano = ano;
		this.faixa = faixa;
		this.minimo = minimo;
		this.maximo = maximo;
		this.deducao = deducao;
		this.aliquota = aliquota;
	}

	public int getAno() {
		return ano;
	}
	
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public int getFaixa() {
		return faixa;
	}
	
	public void setFaixa(int faixa) {
		this.faixa = faixa;
	}
	
	public double getMinimo() {
		return minimo;
	}
	
	public void setMinimo(double minimo) {
		this.minimo = minimo;
	}
	
	public double getMaximo() {
		return maximo;
	}
	
	public void setMaximo(double maximo) {
		this.maximo = maximo;
	}
	
	public double getDeducao() {
		return deducao;
	}
	
	public void setDeducao(double deducao) {
		this.deducao = deducao;
	}
	
	public double getAliquota() {
		return aliquota;
	}
	
	public void setAliquota(double aliquota) {
		this.aliquota = aliquota;
	}
}
