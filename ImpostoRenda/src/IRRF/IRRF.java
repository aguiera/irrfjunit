package IRRF;

import java.util.List;

public class IRRF {

	public double calularIRRF(double baseCalculo) {
		double impostoRF = 0;
		
		if(baseCalculo >= 1903.99 && baseCalculo <= 2826.65) {
			impostoRF = baseCalculo * 0.075 - 142.8;
		} else if (baseCalculo > 2826.65 && baseCalculo <= 3751.05) {
			impostoRF = baseCalculo * 0.15 - 354.8;
		} else if (baseCalculo > 3751.05 && baseCalculo <= 4664.68) {
			impostoRF = baseCalculo * 0.225 - 636.13;
		} else if (baseCalculo > 4664.68) {
			impostoRF = baseCalculo * 0.275 - 869.36;
		}

		return impostoRF;
	}
	
	public double calcularIRRF(double baseCalculo, List<FaixaIR> faixas) {
		double impostoRF = 0;
		
		for(FaixaIR faixa : faixas) {
			if(baseCalculo >= faixa.getMinimo() && (baseCalculo <= faixa.getMaximo() || faixa.getMaximo() == -1)) {
				impostoRF = baseCalculo * faixa.getAliquota() - faixa.getDeducao();
				break;
			}			
		}
				
		return impostoRF;
	}

}
