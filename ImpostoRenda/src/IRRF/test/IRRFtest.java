package IRRF.test;

import static org.junit.Assert.*;

import org.junit.Test;

import IRRF.IRRF;

public class IRRFtest {

	@Test
	public void deveriaCalcularAPrimeiraFaixaIR() {
		double baseCalculo = 1500;
		double impostoEsperado = 0;
		IRRF calculaIR = new IRRF();
		
		double impostoAPagar = calculaIR.calularIRRF(baseCalculo);
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularASegundaFaixaIR() {
		double baseCalculo = 2755.23;
		double impostoEsperado = 63.84;
		IRRF calculaIR = new IRRF();
		
		double impostoAPagar = calculaIR.calularIRRF(baseCalculo);
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	

	@Test
	public void deveriaCalcularATerceiraFaixaIR() {
		double baseCalculo = 3365.32;
		double impostoEsperado = 150;
		IRRF calculaIR = new IRRF();
		
		double impostoAPagar = calculaIR.calularIRRF(baseCalculo);
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularAQuartaFaixaIR() {
		double baseCalculo = 4289.93;
		double impostoEsperado = 329.1;
		IRRF calculaIR = new IRRF();
		
		double impostoAPagar = calculaIR.calularIRRF(baseCalculo);
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularAQuintaFaixaIR() {
		double baseCalculo = 17523.47;
		double impostoEsperado = 3949.59;
		IRRF calculaIR = new IRRF();
		
		double impostoAPagar = calculaIR.calularIRRF(baseCalculo);
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

}
