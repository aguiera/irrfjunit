package IRRF.test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.BeforeClass;
import org.junit.Test;

import IRRF.FaixaIR;
import IRRF.IRRF;

public class IRRFParametrizado {

	private static ArrayList<FaixaIR> faixasIR;
	
	@BeforeClass
	public static void setUp() throws Exception {
		faixasIR = new ArrayList<FaixaIR>();
		
		faixasIR.add(new FaixaIR(2017, 1, 0		 , 1903.98, 0	  , 0));
		faixasIR.add(new FaixaIR(2017, 2, 1903.99, 2826.65, 142.8 , 0.075));
		faixasIR.add(new FaixaIR(2017, 3, 2826.66, 3751.05, 354.8 , 0.15));
		faixasIR.add(new FaixaIR(2017, 4, 3751.06, 4664.68, 636.13, 0.225));
		faixasIR.add(new FaixaIR(2017, 5, 4664.69, -1     , 869.36, 0.275));

		faixasIR.add(new FaixaIR(2014, 1, 0		 , 1787.77, 0	  , 0));
		faixasIR.add(new FaixaIR(2014, 2, 1787.78, 2679.29, 134.04, 0.075));
		faixasIR.add(new FaixaIR(2014, 3, 2679.30, 3572.43, 335.03, 0.15));
		faixasIR.add(new FaixaIR(2014, 4, 3572.44, 4463.81, 602.96, 0.225));
		faixasIR.add(new FaixaIR(2014, 5, 4463.82, -1     , 826.15, 0.275));	

		faixasIR.add(new FaixaIR(2008, 1, 0		 , 1372.81, 0	  , 0));
		faixasIR.add(new FaixaIR(2008, 2, 1372.82, 2743.25, 205.92, 0.15));
		faixasIR.add(new FaixaIR(2008, 3, 2743.26, -1     , 548.82, 0.275));
	}

	@Test
	public void deveriaCalcularAPrimeiraFaixa2017() {
		double baseCalculo = 1500;
		double impostoEsperado = 0;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2017));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularASegundaFaixa2017() {
		
		double baseCalculo = 2755.23;
		double impostoEsperado = 63.84;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2017));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularATerceiraFaixa2017() {
		
		double baseCalculo = 3365.32;
		double impostoEsperado = 150;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2017));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularAQuartaFaixa2017() {
		
		double baseCalculo = 4289.93;
		double impostoEsperado = 329.1;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2017));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularAQuintaFaixa2017() {
		
		double baseCalculo = 17523.47;
		double impostoEsperado = 3949.59;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2017));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularAPrimeiraFaixa2014() {
		double baseCalculo = 1500;
		double impostoEsperado = 0;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2014));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularASegundaFaixa2014() {
		
		double baseCalculo = 2650.32;
		double impostoEsperado = 64.73;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2014));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularATerceiraFaixa2014() {
		
		double baseCalculo = 3365.32;
		double impostoEsperado = 169.77;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2014));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularAQuartaFaixa2014() {
		
		double baseCalculo = 4289.93;
		double impostoEsperado = 362.27;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2014));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularAQuintaFaixa2014() {
		
		double baseCalculo = 17523.47;
		double impostoEsperado = 3992.80;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2014));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularAPrimeiraFaixa2008() {
		double baseCalculo = 1285.23;
		double impostoEsperado = 0;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2008));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}

	@Test
	public void deveriaCalcularASegundaFaixa2008() {
		
		double baseCalculo = 2542.98;
		double impostoEsperado = 175.53;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2008));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	@Test
	public void deveriaCalcularATerceiraFaixa2008() {
		
		double baseCalculo = 17584.69;
		double impostoEsperado = 4286.97;

		IRRF calculaIR = new IRRF();
		double impostoAPagar = calculaIR.calcularIRRF(baseCalculo, getFaixasIR(2008));
		
		assertEquals(impostoEsperado, impostoAPagar, 0.005);
	}
	
	private List<FaixaIR> getFaixasIR(int ano) {
		Stream<FaixaIR> stream =  faixasIR.stream().
				filter(faixa -> faixa.getAno() == ano);
		return stream.sorted((faixa1, faixa2) -> Long.compare(faixa1.getFaixa(),faixa2.getFaixa())).collect(Collectors.toList());
	}	
}
